#pragma once
#include "Communicator.h"
#include "SqliteDataBase.h"
#include "RequestHandlerFactory.h"

class Server
{
private:

	Communicator m_communicator;
	SOCKET _socket;
	IDataBase * m_database;
	RequestHandlerFactory * m_handlerFactory;

public:
	Server();
	~Server();
	void run();

};