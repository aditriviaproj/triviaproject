#pragma once

#include <iostream>
#include <vector>
#include "resStruct.h"
#include "SqliteDataBase.h"


class LoginManager
{
public:
	LoginManager();
	~LoginManager();

	bool SignUp(std::string username, std::string password, std::string email);
	bool LogIn(std::string username, std::string password);
	void LogOut(std::string username);

	void PrintLoggedUsers(); // for debugging

private:

	std::vector<LoggedUser> ConnectedUsers;
	IDataBase* m_DataBase;

	int FindUserIndex(std::string username);


};