#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory ;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory* handlerFactory);

	bool isRequestRelevant(RequestInfo info);
	RequestResult handleRequest(RequestInfo info);
	
private:
	RequestHandlerFactory * m_handlerFactory;

	RequestResult login(RequestInfo info);
	RequestResult signup(RequestInfo info);

};
