#pragma once

#include <iostream>

#include "SqliteDataBase.h"
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "RoomManager.h"
#include "MenuRequestHandler.h"

class MenuRequestHandler;
class LoginRequestHandler;

class RequestHandlerFactory 
{
public: 
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	MenuRequestHandler* createMenuRequestHandler(std::string username);
	RoomManager& getRoomManager();

private: 
	LoginManager m_loginManager;
	IDataBase* _database;
	RoomManager m_roomManager;

};
