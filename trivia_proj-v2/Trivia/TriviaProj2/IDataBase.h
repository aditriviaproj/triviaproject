#pragma once

#include <iostream>
#include <string>
#include <list>

class IDataBase
{
public:
	virtual bool DoesUserExist(std::string username) = 0;
	virtual bool DoesPasswordFit(std::string username, std::string password) = 0 ;
	virtual void AddUser(std::string username, std::string password, std::string email) = 0;

	virtual std::list<Question> getQuestions(int num) = 0;
};