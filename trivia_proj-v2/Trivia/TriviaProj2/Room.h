#pragma once

#include <iostream>
#include <vector>

#include "resStruct.h"

class Room
{
public:
	Room(LoggedUser user, RoomData data);
	bool addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	RoomData GetRoomData();
	std::string getAdmin();

private:
	std::string admin;
	RoomData m_metadata;
	std::vector<LoggedUser>	m_users;

	int FindUserIndex(std::string username);
};