#include "LoginManager.h"


LoginManager::LoginManager()
{
	m_DataBase = new SqliteDataBase();
}

LoginManager::~LoginManager()
{
}

bool LoginManager::SignUp(std::string username, std::string password, std::string email)
{
	if (m_DataBase->DoesUserExist(username))
	{
		std::cerr << username << " already exist! \n"; // switch to exception
		return false;
	}
	else
	{
		m_DataBase->AddUser(username, password, email);
		return true;
	}
}

bool LoginManager::LogIn(std::string username, std::string password)
{
	if (m_DataBase->DoesUserExist(username) && m_DataBase->DoesPasswordFit(username, password))
	{
		LoggedUser user = (username);
		ConnectedUsers.push_back(user);
		return true;
	}
	else
	{
		return false;
	}
}

void LoginManager::LogOut(std::string username)
{
	int index = FindUserIndex(username);
	if (index != -1)
		ConnectedUsers.erase(ConnectedUsers.begin() + index);
}

int LoginManager::FindUserIndex(std::string username)
{
	for (int i = 0; i < ConnectedUsers.size(); i++)
	{
		if (!(username.compare(ConnectedUsers[i].GetUsername())))
			return i;
	}
	return -1;
}

void LoginManager::PrintLoggedUsers()
{
	for (int i = 0; i < ConnectedUsers.size(); i++)
	{
		std::cerr << i + 1 << ". " << ConnectedUsers[i].GetUsername() << "\n";
	}
}
