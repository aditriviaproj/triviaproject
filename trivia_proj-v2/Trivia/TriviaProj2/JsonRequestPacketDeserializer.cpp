#include "JsonRequestPacketDeserializer.h"
#include <string>
# include <iostream>
#include "json.hpp"

using json = nlohmann::json;

LoginRequest JsonRequestPacketDeserializer::DeserializeLoginRequest(std::vector<unsigned char> Buffer)
{
    json a = json::parse(Buffer);
    LoginRequest Final;

    Final.password = a["password"];
    Final.username = a["username"];

    return Final;
}

SignupRequest JsonRequestPacketDeserializer::DeserializeSignUpRequest(std::vector<unsigned char> Buffer)
{
    json a = json::parse(Buffer);
    SignupRequest Final;

    Final.email = a["email"];
    Final.password = a["password"];
    Final.username = a["username"];

    return Final;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::vector<unsigned char> Buffer)
{
    json a = json::parse(Buffer);
    GetPlayersInRoomRequest x; 
    x.roomId =  a["ID"] ;
    return x; 
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> Buffer)
{
    json a = json::parse(Buffer);
    JoinRoomRequest x;
    x.roomId = a["ID"];
    return x;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> Buffer)
{
    json a = json::parse(Buffer);
    CreateRoomRequest x;
    x.roomName = a["RoomName"];
    x.answerTimeout = a["Timeout"];
    x.maxUsers = a["MaxUsers"];
    x.questionCount = a["QuestionCount"];
    return x;
}
