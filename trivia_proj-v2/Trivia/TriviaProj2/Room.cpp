#include "Room.h"

int Room::FindUserIndex(std::string username)
{
	for (int i = 0; i < m_users.size(); i++)
	{
		if (!(username.compare(m_users[i].GetUsername())))
			return i;
	}
	return -1;
}


Room::Room(LoggedUser user ,RoomData data)
{
	admin = user.GetUsername();
	m_users.push_back(user);
	m_metadata = data;
}

bool Room::addUser(LoggedUser user)
{
	if (this->m_metadata.maxPlayers > m_users.size())
	{
		m_users.push_back(user);
		return true;
	}
	return false;
}

void Room::removeUser(LoggedUser user)
{
	int index = FindUserIndex(user.GetUsername());
	if (index != -1)
		m_users.erase(m_users.begin() + index);
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> lst;
	for (int i = 0; i < m_users.size(); i++)
		lst.push_back(m_users[i].GetUsername());
	return lst;
}

RoomData Room::GetRoomData()
{
	return m_metadata;
}

std::string Room::getAdmin()
{
	return this->admin;
}
