#include "LoginRequestHandler.h"

#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handlerFactory)
{
    m_handlerFactory = handlerFactory;
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo info )
{
    if(info.id != 0 && info.id != 1)
        return false;

    return true;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
    if (info.id == 1) 
    {
        return login(info);
    }
    else if (info.id == 0)
    {
        return signup(info);
    }
}

RequestResult LoginRequestHandler::login(RequestInfo info)
{
    RequestResult temp;
    try {
        LoginResponse res; 
        res.status = 1;
        LoginRequest temp2 = JsonRequestPacketDeserializer::DeserializeLoginRequest(info.Buffer);
       
        if (m_handlerFactory->getLoginManager().LogIn(temp2.username, temp2.password))
        {
            temp.Buffer = JsonResponsePacketSerializer::serializeResponse(res);
            temp.NewHandler = m_handlerFactory->createMenuRequestHandler(temp2.username);//new MenuRequestHandler(temp2.username);
        }
        else 
        {
            ErrorResponse a;
            a.status = 2;
            temp.Buffer = JsonResponsePacketSerializer::serializeResponse(a);
            temp.NewHandler = nullptr;
        }
    }
    catch (...)
    {
        ErrorResponse a;
        a.status = 2;
        temp.Buffer = JsonResponsePacketSerializer::serializeResponse(a);
        temp.NewHandler = nullptr;
    }

    return temp;
}

RequestResult LoginRequestHandler::signup(RequestInfo info)
{
    RequestResult temp;
    try {
        SignupResponse res;
        res.status = 1;
        SignupRequest temp2 = JsonRequestPacketDeserializer::DeserializeSignUpRequest(info.Buffer);
        if (m_handlerFactory->getLoginManager().SignUp(temp2.username, temp2.password, temp2.email))
        {
            temp.Buffer = JsonResponsePacketSerializer::serializeResponse(res);
            temp.NewHandler = m_handlerFactory->createMenuRequestHandler(temp2.username);
        }
        else
        {
            throw ("error");
        }
    }
    catch (...)
    {
        ErrorResponse a;
        a.status = 2;
        temp.Buffer = JsonResponsePacketSerializer::serializeResponse(a);
        temp.NewHandler = nullptr;
    }

    return temp;
}
