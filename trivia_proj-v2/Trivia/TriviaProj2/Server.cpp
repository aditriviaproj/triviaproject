#include "Server.h"

#include "Communicator.h"
#include <iostream>
#include <vector>
#include <condition_variable>
#include <WinSock2.h>



Server::Server()
{
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	m_communicator = Communicator();
	this->m_communicator.set_m_serverSocket(_socket);
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void Server::run()
{
	std::string input_user;
	std::thread t_connector(&Communicator::startHandleRequests, m_communicator);
	t_connector.detach();

	while (input_user != "exit")
	{
		std::cout << "enter exit to end the connection or any other key to continue: ";
		std::cin >> input_user;


	}

}

