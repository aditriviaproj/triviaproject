#include "RoomManager.h"

void RoomManager::createRoom(LoggedUser user, RoomData data)
{
	Room a(user, data);
	m_rooms.insert(std::pair<unsigned int, Room>(data.id, a));
}

void RoomManager::deleteRoom(int ID)
{
	m_rooms.erase(ID);
}

unsigned int RoomManager::getRoomState(int ID)
{
	auto it = m_rooms.find(ID);

	if (it != m_rooms.end())
		return it->second.GetRoomData().isActive;

	return 0; // change to exception 
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> values; 
	for (std::map<int, Room>::iterator it = m_rooms.begin(); it != m_rooms.end(); ++it) {
		values.push_back(it->second.GetRoomData());		
	}
	return values;
}

std::map<int, Room> RoomManager::get_m_rooms()
{
	return this->m_rooms;
}

RoomData RoomManager::GetRoomDataById(unsigned int id)
{
	auto it = m_rooms.find(id);
	if (it != m_rooms.end())
		return (it->second.GetRoomData());
}

Room* RoomManager::_GetRoomById(unsigned int id)
{
	auto it = m_rooms.find(id);
	if (it != m_rooms.end())
		return &(it->second);
}


