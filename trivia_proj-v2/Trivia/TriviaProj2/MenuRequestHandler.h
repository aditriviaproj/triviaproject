#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "json.hpp"

class RequestHandlerFactory;

using json = nlohmann::json;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser * user, RequestHandlerFactory* m_handlerFactory);

	bool isRequestRelevant(RequestInfo info) ;
	RequestResult handleRequest(RequestInfo info) ;
	
	RequestResult signout(RequestInfo info);
	RequestResult getRooms(RequestInfo info);
	RequestResult getPlayersInRoom(RequestInfo info);
	RequestResult joinRoom(RequestInfo info);
	RequestResult createRoom(RequestInfo info);
	RequestResult CloseRoom(RequestInfo info);
	RequestResult StartGame(RequestInfo info);
	RequestResult GetRoomState(RequestInfo info);
	RequestResult LeaveRoom(RequestInfo info);
	json RoomDataToJson(RoomData data);
private:

	unsigned int getId();
	LoggedUser * m_user;
	RequestHandlerFactory * m_handlerFactory;



};
