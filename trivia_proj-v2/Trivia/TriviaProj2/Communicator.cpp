#include "Communicator.h"
#include <iostream>       
#include <thread> 
#include <string>
#include "LoginRequestHandler.h"
#include "json.hpp"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "resStruct.h"

static const unsigned short PORT = 65002;
static const unsigned int IFACE = 0;

using json = nlohmann::json;


void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded";

	if (::listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "listening...";
}

void Communicator::acceptClient()
{
	SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted !";
	// create new thread for client	and detach from it
	std::thread tr(&Communicator::handleNewClient, this, client_socket);
	tr.detach();
	
}

void Communicator::handleNewClient(SOCKET client_socket)
{
	std::vector<unsigned char> buff;
	RequestInfo temp;
	RequestResult res;
	int data_len;
	char msg_code;
	char* msg = nullptr;
	char* len = nullptr;
	char* data = nullptr;
	std::vector<unsigned char> dataUC ;
	set_m_clients(client_socket, m_handlerFactory->createLoginRequestHandler());

	while (m_clients[client_socket] != nullptr)
	{
		msg = get_msg(client_socket, 1, 0);
		len = get_msg(client_socket, 4, 0);
		memcpy(&msg_code, msg, sizeof(char));
		memcpy(&data_len, len, sizeof(int));

		if (data_len != 0)
		{
			data = get_msg(client_socket, data_len, 0);
			std::vector<char> dataC(data, data + data_len);
			std::copy(dataC.begin(), dataC.end(), std::back_inserter(dataUC));
			dataC.clear();
		}

		temp.Buffer = dataUC;
		temp.id = (int)msg_code;
		temp.RecivalTime = std::time(0);

		if (m_clients[client_socket]->isRequestRelevant(temp))
		{
			res = m_clients[client_socket]->handleRequest(temp);
		}

		if (res.NewHandler != nullptr)
		{
			m_clients[client_socket] = res.NewHandler;
		}
		if (send(client_socket, (char*)(&res.Buffer[0]), res.Buffer.size(), 0) == INVALID_SOCKET)
		{
			std::cerr << "Error while sending message to client";
		}
		data_len = 0;
		dataUC.clear();

	}

}

void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accepting client...\n";
		acceptClient();
	}
}

void Communicator::set_m_serverSocket(SOCKET ServerS)
{
	m_serverSocket = ServerS;
}


Communicator::Communicator()
{
	m_handlerFactory = new RequestHandlerFactory();
}

Communicator::~Communicator()
{
}

void Communicator::set_m_clients(SOCKET clientS, IRequestHandler* irequestHandler)
{
	m_clients.insert(std::pair<SOCKET, IRequestHandler*>(clientS, irequestHandler));
}

char* Communicator::get_msg(SOCKET sc, int bytesNum, int flags)
{

	char* data = new char[bytesNum];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}


	return data;
}