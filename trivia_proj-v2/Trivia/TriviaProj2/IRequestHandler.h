#pragma once

#include <iostream>
#include <ctime>
#include <vector>

struct RequestInfo;
struct RequestResult;

class IRequestHandler
{
public:
	IRequestHandler();

	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;
};

/*
0 - signup 
1 - login 
2 - signout 
3 - getRooms
4 - getPlayersInRoom
5 - joinRoom
6 - createRoom
*/

struct RequestInfo
{
	int id;
	std::time_t RecivalTime; 
	std::vector<unsigned char> Buffer;
};

struct RequestResult
{
	std::vector<unsigned char> Buffer;
	IRequestHandler* NewHandler;
};
