#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "Room.h"

class RoomManager
{
public:
	void createRoom(LoggedUser user, RoomData data);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<RoomData> getRooms();
	std::map<int, Room> get_m_rooms();
	RoomData GetRoomDataById(unsigned int id);
	Room* _GetRoomById(unsigned int id);
	
private:
	std::map<int, Room> m_rooms;
	
};
