#include <iostream>
#include "Communicator.h"
#include "resStruct.h"


class JsonResponsePacketSerializer
{

public:
	static std::vector<unsigned char> serializeResponse(ErrorResponse ErrorRes);
	static std::vector<unsigned char> serializeResponse(LoginResponse loginRes);
	static std::vector<unsigned char> serializeResponse(SignupResponse signupRes);
	//static std::vector<unsigned char> serializeResponse_logout();
	static std::vector<unsigned char> serializeResponse(LogoutResponse LogRes);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse RoomRes);
	static std::vector<unsigned char> serializerResponse(GetPlayersInRoomResponse PlayerInRoomRes);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse JoinRoomRes);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse CreateRoomRes);
		

};