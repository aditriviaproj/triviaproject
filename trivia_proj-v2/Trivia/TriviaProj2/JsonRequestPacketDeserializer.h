#pragma once
#include "resStruct.h"
#include <vector>
#include "json.hpp"


class JsonRequestPacketDeserializer
{
public:

	static LoginRequest DeserializeLoginRequest(std::vector<unsigned char> Buffer);
	static SignupRequest DeserializeSignUpRequest(std::vector<unsigned char> Buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::vector<unsigned char> Buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> Buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> Buffer);

};