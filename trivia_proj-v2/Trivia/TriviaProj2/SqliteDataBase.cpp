#include "SqliteDataBase.h"
#include <vector>


std::vector<User> UserCallBackVec;
std::vector<Question> QuestionCallBackVec;

//-------Constractor & Destrtactor-------//

SqliteDataBase::SqliteDataBase()
{
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cerr << "Failed to open DB";
		throw std::string("Failed to open DB");
	}
	if (doesFileExist != 0) {
		std::cerr << "Database doent exist";
		throw std::string("Database doent exist");
	}
}

SqliteDataBase::~SqliteDataBase()
{
	sqlite3_close(db);
	db = nullptr;
}


//-------CallBack functions-------//


int UserCallBack(void* data, int argc, char** argv, char** azColName)
{
	User a;
	a.username = "";
	std::string username = "", password = "", email = "";
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "username") {
			a.username = argv[i];
		}
		else if (std::string(azColName[i]) == "password") {
			a.password = (argv[i]);
		}
		else if (std::string(azColName[i]) == "email") {
			a.email = (argv[i]);
		}
	}

	if (a.username != "")
		UserCallBackVec.push_back(a);

	return 0;

}

int QuestionCallBack(void* data, int argc, char** argv, char** azColName)
{
	Question a;
	a.que = "";
	std::string username = "", password = "", email = "";
	for (int i = 0; i < argc; i++) {

		if (std::string(azColName[i]) == "que") {
			a.que = argv[i];
		}
		else if (std::string(azColName[i]) == "correct") {
			a.Correct = (argv[i]);
		}
		else if (std::string(azColName[i]) == "wrong1") {
			a.Wrong[0] = (argv[i]);
		}
		else if (std::string(azColName[i]) == "wrong2") {
			a.Wrong[1] = (argv[i]);
		}
		else if (std::string(azColName[i]) == "wrong3") {
			a.Wrong[2] = (argv[i]);
		}
	}

	if (a.que != "")
		QuestionCallBackVec.push_back(a);

	return 0;
}

//-------Class functions-------//

bool SqliteDataBase::DoesUserExist(std::string username)
{// select * from Users where username is ;

	UserCallBackVec.clear();
	std::string rq = "";
	char* errMessage = nullptr;
	int res = 0;

	rq = "select * from Users where username = '" + username + "' ; ";
	res = sqlite3_exec(db, rq.c_str(), UserCallBack, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "\n!\nERROR FROM DoesUserExist func\n" << errMessage << "\n!\n";
	}
	if (UserCallBackVec.empty())
		return false;

	UserCallBackVec.clear();
	return true;
}

bool SqliteDataBase::DoesPasswordFit(std::string username, std::string password)
{
	UserCallBackVec.clear();
	std::string rq = "";
	char* errMessage = nullptr;
	int res = 0;

	rq = "select * from Users where username = '" + username + "'  and password = '" + password + "' ; ";
	res = sqlite3_exec(db, rq.c_str(), UserCallBack, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "\n!\nERROR FROM DoesUserExist func\n" << errMessage << "\n!\n";
	}
	if (UserCallBackVec.empty())
		return false;

	UserCallBackVec.clear();
	return true;
}

void SqliteDataBase::AddUser(std::string username, std::string password, std::string email)
{
	std::string rq = "";
	char* errMessage = nullptr;
	int res = 0;
	rq = "begin ; ";
	res = sqlite3_exec(db, rq.c_str(), nullptr, nullptr, &errMessage);

	rq = "INSERT INTO Users VALUES( '" + username + "', '" + password + "' , '" + email + "');";

	res = sqlite3_exec(db, rq.c_str(), nullptr, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cout << "\n!\nFALIED TO ADD USER\n" << errMessage << "!\n\n";
		rq = "rollback ; ";
		res = sqlite3_exec(db, rq.c_str(), nullptr, nullptr, &errMessage);
		return;
	}
	rq = "commit ; ";
	res = sqlite3_exec(db, rq.c_str(), nullptr, nullptr, &errMessage);
}

std::list<Question> SqliteDataBase::getQuestions(int num)
{
	UserCallBackVec.clear();
	std::list<Question> lst;
	std::string rq = "";
	char* errMessage = nullptr;
	int res = 0;

	rq = "select * from Questions ; ";
	res = sqlite3_exec(db, rq.c_str(), QuestionCallBack, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "\n!\nERROR FROM DoesUserExist func\n" << errMessage << "\n!\n";
	}
	if (QuestionCallBackVec.empty())
		std::cerr << "No Questions found";

	for (int i = 0; i < QuestionCallBackVec.size(); i++)
	{
		lst.push_back(QuestionCallBackVec[i]);
		if (i == num - 1)
			return lst;
	}

	QuestionCallBackVec.clear();
	return lst;
}

void SqliteDataBase::PrintLst(std::list<Question> lst)
{
	for (auto it = lst.begin(); it != lst.end(); it++)
	{
		std::cerr << "Q: "  << it->que << "\n1. " << it->Correct << "\n2. " << it->Wrong[0] << "\n3. " << it->Wrong[1] << "\n4. " << it->Wrong[3] << "\n"; 
	}
}
