#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <map>
//#include "map"
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <vector>

class Communicator
{
private:
	char* get_msg(SOCKET sc, int bytesNum, int flags);
	void bindAndListen();
	void acceptClient();
	void handleNewClient(SOCKET client_socket);
	//void handleReceivedMessages();
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory * m_handlerFactory;

public:

	void startHandleRequests();
	void set_m_serverSocket(SOCKET ServerS);
	Communicator();
	~Communicator();
	void set_m_clients(SOCKET clientS, IRequestHandler* irequestHandler);
};