#include "MenuRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include <list>
#include <stdio.h>
#include <stdlib.h>
#include <iterator>
#include "json.hpp"
#include <time.h>

using json = nlohmann::json;
using namespace std;
/*
0 - signup
1 - login
2 - signout
3 - getRooms
4 - getPlayersInRoom
5 - joinRoom
6 - createRoom
7 - CloseRoom
8 - StartGame
9 - GetRoomState
10 - LeaveRoom

*/


MenuRequestHandler::MenuRequestHandler(LoggedUser * user, RequestHandlerFactory* m_handlerFactory)
{
    this->m_user = user;
    this->m_handlerFactory = m_handlerFactory;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
    if(info.id != 2 && info.id != 3 && info.id != 4 && info.id != 5 && info.id != 6)
        return false;
    return true;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
    if (info.id == 2)
        return signout(info);
    else if (info.id == 3)
        return getRooms(info);
    else if (info.id == 4)
        return getPlayersInRoom(info);
    else if (info.id == 5)
        return joinRoom(info);
    else if (info.id == 6)
        return createRoom(info);
    else if (info.id == 7)
        return CloseRoom(info);
    else if (info.id == 8)
        return StartGame(info);
    else if (info.id == 9)
        return GetRoomState(info);
    else if (info.id == 10)
        return LeaveRoom(info);
}

RequestResult MenuRequestHandler::signout(RequestInfo info)
{
    char signoutres = '1';
    std::vector<unsigned char> Buffer;
    Buffer.push_back(signoutres);
    m_handlerFactory->getLoginManager().LogOut(m_user->GetUsername());
    RequestResult a; 
    a.NewHandler = m_handlerFactory->createLoginRequestHandler();
    a.Buffer = Buffer;
    return a;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info)
{
    RequestResult a;
    a.NewHandler = this;
    GetRoomsResponse grr;
    grr.rooms = m_handlerFactory->getRoomManager().getRooms();
    grr.status = 1;
    a.Buffer =  JsonResponsePacketSerializer::serializeResponse(grr);
    return a;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info) ////////// !!!! ////////
{
    RequestResult ret;
    GetPlayersInRoomResponse p;

    GetPlayersInRoomRequest a = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(info.Buffer);
    Room* room = m_handlerFactory->getRoomManager()._GetRoomById(a.roomId);
    std::vector<std::string> users = room->getAllUsers();
    p.players = users;
    p.admin = room->getAdmin();
    ret.Buffer = JsonResponsePacketSerializer::serializerResponse(p);
    ret.NewHandler = this;
    return ret;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
    RequestResult ret;
    JoinRoomResponse _joinRoomResponse;
    _joinRoomResponse.status = 1;
    JoinRoomRequest jr = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.Buffer);
    Room* room = m_handlerFactory->getRoomManager()._GetRoomById(jr.roomId);
    if (!room->addUser(*m_user))
    {
        ErrorResponse error;
        error.status = 1;
        ret.Buffer = JsonResponsePacketSerializer::serializeResponse(error);
    }
    else
    {
        ret.Buffer = JsonResponsePacketSerializer::serializeResponse(_joinRoomResponse);
    }
    ret.NewHandler = this;
    return ret;
    
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
    RequestResult ret;
    CreateRoomResponse cr;
    cr.status = 1;
    std::vector<unsigned char> tempBuf;
    cr.ID = getId();
    CreateRoomRequest a = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.Buffer);
    RoomData _roomData;
    _roomData.id = cr.ID;
    _roomData.name = a.roomName;
    _roomData.maxPlayers = a.maxUsers;
    _roomData.numOfQuestionsInGame = a.questionCount;
    m_handlerFactory->getRoomManager().createRoom(*m_user, _roomData);
    ret.Buffer = JsonResponsePacketSerializer::serializeResponse(cr);
    ret.NewHandler = this;
    return ret;
}

RequestResult MenuRequestHandler::CloseRoom(RequestInfo info)
{
    return RequestResult();
}

RequestResult MenuRequestHandler::StartGame(RequestInfo info)
{
    return RequestResult();
}

RequestResult MenuRequestHandler::GetRoomState(RequestInfo info)
{
    return RequestResult();
}

RequestResult MenuRequestHandler::LeaveRoom(RequestInfo info)
{
    return RequestResult();
}

json MenuRequestHandler::RoomDataToJson(RoomData data)
{
    json j;
    j["ID"] = data.id;
    j["isActive"] = data.isActive;
    j["maxPlayers"] = data.maxPlayers;
    j["name"] = data.name;
    j["numOfQuestionsInGame"] = data.numOfQuestionsInGame;
    j["timePerQuestion"]  = data.timePerQuestion;
    return json();
}


unsigned int MenuRequestHandler::getId()
{
    unsigned int id;
    std::map<int, Room> m_rooms = m_handlerFactory->getRoomManager().get_m_rooms();
    srand(time(NULL));
    while (true)
    {
        id = rand() % 30 + 1985;   // id's in the range 1985-2014 
        auto it = m_rooms.find(id);
        if (it == m_rooms.end() || m_rooms.size() == 0)
            return id;
    }
}


