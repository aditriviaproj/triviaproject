#pragma once
#include <iostream>
#include <vector>
#include <string>

class LoggedUser
{
public:

	LoggedUser(std::string username) { m_username = username; }
	std::string GetUsername() { return m_username; }

private:

	std::string m_username;
};


struct User
{
	std::string username;
	std::string password;
	std::string email;
};

//---------Login---------//

struct LoginResponse
{
	unsigned int status;
};

struct LoginRequest
{
	std::string username;
	std::string password;
};

//---------Signup---------//

struct SignupResponse
{
	unsigned int status;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};



//---------Logout---------//

struct LogoutResponse
{
	unsigned int status;
};

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

//---------GetRooms---------//

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

//---------GetPlayersInRoom---------//

struct GetPlayersInRoomResponse
{
	std::string admin;
	std::vector<std::string> players;
};


struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

//---------JoinRoom---------//

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomResponse
{
	unsigned int status;
};

//---------CreateRoom---------//

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct CreateRoomResponse
{
	unsigned int status;
	int ID;
};

//---------Others---------//

struct Question
{
	std::string que;
	std::string Wrong[3] ;
	std::string Correct;
};

struct ErrorResponse
{
	unsigned int status;
};



