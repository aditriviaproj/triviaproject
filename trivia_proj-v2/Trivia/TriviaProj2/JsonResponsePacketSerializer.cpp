#include "JsonResponsePacketSerializer.h"
#include "json.hpp"
#include <string>
# include <iostream>
#include <iomanip> 
//#include <nlohmann/json.hpp>  
//using json = nlohmann::json;

const size_t BYTES_PER_INT = sizeof(int);
using json = nlohmann::json;
int intToCharArray(char* buffer, int in);
std::vector<unsigned char> intToBytes(int paramInt);
json RoomdataToJson(RoomData roomData);


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse ErrorRes)
{
    char errorResponse = '2';
    std::vector<unsigned char> Buffer;
    json j;
    j["message"] = "ERROR";


    json::to_bson(j);
    int intLen = j.size();
    std::vector<unsigned char> bJson = json::to_bson(j);

    Buffer.push_back(errorResponse);
    Buffer.push_back(intLen);

    for (int i = 0; i < bJson.size(); i++)
        Buffer.push_back(bJson[i]);
    
    return Buffer;
}



std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse loginRes)
{
    int i = 0;
    char loginResponse = '1';
    std::vector<unsigned char> Buffer;
    json j;
    j["status"] = 1;
    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    std::cout << "\nlen: " << intLen << "!!\n";
    Buffer.push_back(loginResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }

    for ( i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse signupRes)
{
    char signupResponse = '0';
    std::vector<unsigned char> Buffer;
    json j;
    j["status"] = 1;

    json::to_bson(j);
   

    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    Buffer.push_back(signupResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }

    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }

    return Buffer;
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse LogRes)
{
    char logoutResponse = '1';
    std::vector<unsigned char> Buffer;
    json j;
    j["status"] = 1;

    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    Buffer.push_back(logoutResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }
    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse RoomRes)
{
    char GetRoomsResponse = '1';

    std::list<json> jList;
    std::vector<unsigned char> Buffer;
    json j;
    std::string Rooms = "";

    for (int i = 0; i < RoomRes.rooms.size(); i++)
    {
        jList.push_back(RoomdataToJson(RoomRes.rooms[i]));
    }
    j["Rooms"] = jList;

    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    Buffer.push_back(GetRoomsResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }
    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializerResponse(GetPlayersInRoomResponse PlayerInRoomRes)
{
    std::vector<unsigned char> Buffer;
    char GetPlayersInRoomResponse = '1';
    std::list<std::string> Players;

    for (int i = 0; i < PlayerInRoomRes.players.size(); i++)
    {
        Players.push_back(PlayerInRoomRes.players[i]);
    }
   std::string admin = PlayerInRoomRes.admin;
   json j;
   j["admin"] = admin;
   j["players"] = Players;


    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    std::vector<unsigned char> bytesLen = intToBytes(intLen);
    Buffer.push_back(GetPlayersInRoomResponse);
    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }
    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse JoinRoomRes)
{
    char JoinRoomResponse = '1';
    std::vector<unsigned char> Buffer;
    json j;
    j["status"] = 1;

    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    Buffer.push_back(JoinRoomResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }
    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}
    
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse CreateRoomRes)
{
    char CreateRoomResponse = '1';
    std::vector<unsigned char> Buffer;
    json j;
    j["ID"] = CreateRoomRes.ID;
    std::vector<unsigned char> data = json::to_bson(j);
    int intLen = data.size();
    Buffer.push_back(CreateRoomResponse);
    std::vector<unsigned char> bytesLen = intToBytes(intLen);

    for (int i = 0; i < 4; i++)
    {
        Buffer.push_back(bytesLen[i]);
    }

    for (int i = 0; i < intLen; i++)
    {
        Buffer.push_back(data[i]);
    }
    return Buffer;
}

int intToCharArray(char* buffer, int in)
{
    for (size_t i = 0; i < BYTES_PER_INT; i++) {
        size_t shift = 8 * (BYTES_PER_INT - 1 - i);
        buffer[i] = (in >> shift) & 0xff;
    }
    return 0;
}
std::vector<unsigned char> intToBytes(int paramInt)
{
    std::vector<unsigned char> arrayOfByte(4);
    for (int i = 0; i < 4; i++)
        arrayOfByte[3 - i] = (paramInt >> (i * 8));
    return arrayOfByte;
}

json RoomdataToJson(RoomData roomData)
{
    json j;
    j["id"] = roomData.id;
    j["name"] = roomData.name;
    j["maxPlayers"] = roomData.maxPlayers;
    j["numOfQuestionsInGame"] = roomData.numOfQuestionsInGame;
    j["timePerQuestion"] = roomData.timePerQuestion;
    j["isActive"] = roomData.isActive;
    return j;
}