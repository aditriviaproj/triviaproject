#pragma once

#include "sqlite3.h"
#include "resStruct.h"
#include "IDataBase.h"
#include <iostream>
#include <io.h>
#include <vector>
#include <exception>



class SqliteDataBase : public IDataBase
{
public:
	SqliteDataBase();
	~SqliteDataBase();

	bool DoesUserExist(std::string username);
	bool DoesPasswordFit(std::string username, std::string password);
	void AddUser(std::string username, std::string password, std::string email);

	std::list<Question> getQuestions(int num);

	void PrintLst(std::list<Question> lst); // for debugging 
private:

	//-------Varibles-------//

	sqlite3* db;
	std::string dbFileName = "ProjDB.sqlite";


};
