﻿using System;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Client
{
    public class RoomData
    {
        public uint id { get; set; }
        public string name { get; set; }
        public uint maxPlayers { get; set; }
        public uint numOfQuestionsInGame { get; set; }
        public uint timePerQuestion { get; set; }
        public uint isActive { get; set; }
    }
    public class LoginReq
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class SignUpReq
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }
    public class CreateRoomRequest
    {
        public string RoomName { get; set; }
        public uint MaxUsers { get; set; }
        public uint QuestionCount { get; set; }
        public uint Timeout { get; set; }
    }
    public class JoinRoomReq
    {
        public int ID { get; set; }
    }
    public class getplayersINRoom
    {
        public uint ID { get; set; }
    }
    class Serializer
    {
        public static byte[] MergeThreeArray(byte a1, byte[] a2, byte[] a3)
        {
            byte[] rv = new byte[1 + a2.Length + a3.Length];
            rv[0] = a1;
            System.Buffer.BlockCopy(a2, 0, rv, 1, a2.Length);
            System.Buffer.BlockCopy(a3, 0, rv, 1 + a2.Length, a3.Length);
            return rv;
        }
        public static byte[] JsonStringToByteArray(string jsonString)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetBytes(jsonString);
        }
        public static byte[] SerializeLogout()
        {
            byte[] ret = new byte[5];
            ret[0] = 2;
            ret[1] = 0;
            ret[2] = 0;
            ret[3] = 0;
            ret[4] = 0;
            return ret;
        }
        public static byte[] SerializerGetPlayersInRoom(getplayersINRoom a)
        {
            byte code = 4;
            var getPlinRoomReq = new getplayersINRoom { ID = a.ID};
            string str = JsonSerializer.Serialize(getPlinRoomReq);
            byte[] msg = JsonStringToByteArray(str);
            byte[] MsgLen = BitConverter.GetBytes(msg.Length);
            return MergeThreeArray(code, MsgLen, msg);
        }
   
        public static byte[] SerializerCreateRoom(CreateRoomRequest NewRoom )
        {
            byte code = 6;
            var CreRoomReq = new CreateRoomRequest { RoomName = NewRoom.RoomName, QuestionCount = NewRoom.QuestionCount, MaxUsers = NewRoom.MaxUsers, Timeout = NewRoom.Timeout };
            string str = JsonSerializer.Serialize(CreRoomReq);
            byte[] msg = JsonStringToByteArray(str);
            byte[] MsgLen = BitConverter.GetBytes(msg.Length);
            // if (BitConverter.IsLittleEndian)
            //   Array.Reverse(MsgLen);
            return MergeThreeArray(code, MsgLen, msg);
        }
        public static byte[] SerializerGetRooms()
        {
            byte[] ret = new byte[5];
            ret[0] = 3;
            ret[1] = 0;
            ret[2] = 0;
            ret[3] = 0;
            ret[4] = 0;
            return ret;
        }
        public static byte[] SerializerJoinRoom(int _id)
        {
            
                byte code = 5; // login code
                var JoinRoomReq = new JoinRoomReq { ID = _id };
                string str = JsonSerializer.Serialize(JoinRoomReq);
                byte[] msg = JsonStringToByteArray(str);
                byte[] MsgLen = BitConverter.GetBytes(msg.Length);
                // if (BitConverter.IsLittleEndian)
                //   Array.Reverse(MsgLen);
                return MergeThreeArray(code, MsgLen, msg);
        }
        public static byte[] SerializerLogin(string m_username , string m_password)
        {
            byte code = 1; // login code
            var LoginReq = new LoginReq { username = m_username, password = m_password };
            string str = JsonSerializer.Serialize(LoginReq);
            byte[] msg = JsonStringToByteArray(str);
            byte[] MsgLen = BitConverter.GetBytes(msg.Length);
           // if (BitConverter.IsLittleEndian)
             //   Array.Reverse(MsgLen);
            return MergeThreeArray(code, MsgLen, msg);
        }
        public static byte[] SerializerSignUp(string m_username , string m_password, string m_email)
        {
            byte code = 0; // signup code
            var signUp = new SignUpReq { username = m_username, password = m_password , email = m_email};
            string str = JsonSerializer.Serialize(signUp);
            byte[] msg = JsonStringToByteArray(str);
            byte[] MsgLen = BitConverter.GetBytes(msg.Length);
            return MergeThreeArray(code, BitConverter.GetBytes(msg.Length), msg);
        }
        public static byte[] SerializerJoinRoom(uint id)
        {
            return new byte[1];
        }
    }
}
