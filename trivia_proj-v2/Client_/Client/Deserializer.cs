﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using Newtonsoft.Json;
using System.Windows;
using System.IO;
using Json.Net;
using Newtonsoft.Json.Bson;
using System.Text.Json;
using System.Text.Json.Serialization;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace Client
{
    public class getRoomRes
    {
        public getRoomRes(List<RoomData> Rooms)
        {
            this.Rooms = Rooms;
        }

        public List<RoomData> Rooms { get; set; }
    }
    public class ReqRes // res for - Login , Sign up , log out, join room, create room 
    {
        public int status { get; set; }
    }

    public class GetRoomsReq
    {
        public List<RoomData> Rooms { get; set; }
    }
    public class GetplayersInRoomsReq
    {
        public string admin { get; set; }
        public List<string> players { get; set; }
    }
    public class createRoom
    {
        public int ID { get; set; }

    }

    class Deserializer
    {
        
        public static createRoom DeserializeCreateRoom(byte [] buffer)
        {
             int i = 0, k = 0;
            byte MsgCode = buffer[0];
            int msgCodeI = Convert.ToInt32(MsgCode);
            byte[] MsgLen = new byte[4];
            for (i = 0; i < MsgLen.Length; i++)
            {
                MsgLen[i] = buffer[i + 1];
            }
            Array.Reverse(MsgLen);
            int data_len = BitConverter.ToInt32(MsgLen, 0);
            byte[] msg = new byte[data_len];
            for (i = 5; i < data_len + 4; i++)
            {
                msg[k] = buffer[i];
                k++;
            }
            MemoryStream ms = new MemoryStream(msg);
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                createRoom createRoom_ = serializer.Deserialize<createRoom>(reader);
                return createRoom_;
            }

        }
        public static getRoomRes DeserializeGetRooms(byte[] buffer)
        {
            int i = 0, k = 0;
            byte MsgCode = buffer[0];///covert to char
            int msgCodeI = Convert.ToInt32(MsgCode);
            byte[] MsgLen = new byte[4];
            for (i = 0; i < MsgLen.Length; i++)
            {
              MsgLen[i] = buffer[i + 1];
            }
            Array.Reverse(MsgLen);
            int data_len = BitConverter.ToInt32(MsgLen, 0);
            char char_len = BitConverter.ToChar(MsgLen,0); 
            byte[] msg = new byte[data_len];
            for (i = 5; i <= data_len + 4; i++)
            {
                msg[k] = buffer[i];
                k++;
            }            

            MemoryStream ms = new MemoryStream(msg);
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                getRoomRes gRoomRes = serializer.Deserialize<getRoomRes>(reader);
                return gRoomRes;
            }
           
        }
        public static byte DeserializeLogin(byte[] buffer)
        {

            byte MsgCode = buffer[0];///covert to char
            return MsgCode;
        }
        public static byte DeserializeSignUp(byte[] buffer)
        {
            byte MsgCode = buffer[0];///covert to char
            return MsgCode;
        }
        public static GetplayersInRoomsReq DeserializeGetPlayersInRooms(byte[] buffer)
        {
            int i = 0, k = 0;
            byte MsgCode = buffer[0];///covert to char
            int msgCodeI = Convert.ToInt32(MsgCode);
            byte[] MsgLen = new byte[4];
            for (i = 0; i < MsgLen.Length; i++)
            {
                MsgLen[i] = buffer[i + 1];
            }
            Array.Reverse(MsgLen);
            int data_len = BitConverter.ToInt32(MsgLen, 0);
            char char_len = BitConverter.ToChar(MsgLen, 0);
            byte[] msg = new byte[data_len];
            for (i = 5; i <= data_len + 4; i++)
            {
                msg[k] = buffer[i];
                k++;
            }

            MemoryStream ms = new MemoryStream(msg);
            using (BsonReader reader = new BsonReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                GetplayersInRoomsReq gPlayersRes = serializer.Deserialize<GetplayersInRoomsReq>(reader);
                return gPlayersRes;
            }


        }
        public static byte DeserializeJoin(byte[] buffer)
        {
            byte MsgCode = buffer[0];///covert to char
            return MsgCode;
        }

    }
}
