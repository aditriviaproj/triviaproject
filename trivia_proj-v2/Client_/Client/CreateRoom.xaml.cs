﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Window
    {
        public Communicator _com { get; set; }
        public CreateRoom(Communicator _com)
        {
            this._com = _com;
            InitializeComponent();
        }
        
        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            int Qtime = 0 , Qnum = 0 , Pnum = 0 ; 
            if (!string.IsNullOrWhiteSpace(RoomName.Text) && !string.IsNullOrWhiteSpace(PlayerNum.Text) && !string.IsNullOrWhiteSpace(QuestionTime.Text) && !string.IsNullOrWhiteSpace(QuestionNum.Text))
            {
                if(int.TryParse(QuestionTime.Text, out Qtime) && int.TryParse(QuestionNum.Text, out Qnum) && int.TryParse(PlayerNum.Text, out Pnum))
                {
                    CreateRoomRequest a = new CreateRoomRequest();
                    a.RoomName = RoomName.Text;
                    a.QuestionCount = (uint)Qnum;
                    a.MaxUsers = (uint)Pnum;
                    a.Timeout = (uint)Qtime;
                    
                    byte[] send = Serializer.SerializerCreateRoom(a);
                    createRoom cr = Deserializer.DeserializeCreateRoom(_com.SendMsg(send));
                        this.Close();
                        InRoom wnd = new InRoom(_com, cr.ID);
                        wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Enter numbers!");
                }
            }
            else
            {
                MessageBox.Show("Please enter all the requiered data!");
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new LoggedUser(_com).ShowDialog();
        }

    }
}
