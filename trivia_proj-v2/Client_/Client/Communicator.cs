﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json.Serialization;


namespace Client
{
    public class Communicator
    {
        public Socket sender;
        public Communicator()
        {

            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[1];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 65002);

            // Create a TCP/IP  socket.
           sender = new Socket(ipAddress.AddressFamily,
           SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.

             sender.Connect(remoteEP);

           } 

        public byte[] SendMsg(byte[] Buffer_)
        {
            byte[] buffer = new byte[4096];
            int bytesSent = sender.Send(Buffer_);
            int bytesRec = sender.Receive(buffer);
            byte[] buf = new byte[bytesRec];
            Buffer.BlockCopy(buffer, 0, buf, 0, bytesRec);
            return buf; 
        }


    }
}
