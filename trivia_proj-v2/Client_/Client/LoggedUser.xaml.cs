﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{

    public partial class LoggedUser : Window
    {
       

        public string Text { get; set; }
        public Communicator _com { get; set; }

        /*
         <TextBlock  Margin="283,88,116,331"> 
                 <Span FontWeight="Bold" FontSize="40" Foreground="White">Hello! user</Span>
        </TextBlock>*/
        public LoggedUser(Communicator Com)
        {
            _com = Com;
            InitializeComponent();           
        }
        private void Signout_Click(object sender, RoutedEventArgs e)
        {
            byte[] resb = _com.SendMsg(Serializer.SerializeLogout());
            this.Close();
            new MainWindow().ShowDialog();
        }
        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new JoinRoom(_com).ShowDialog();
        }
        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new CreateRoom(_com).ShowDialog();
        }
        private void MyStatus_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not implemented");
        }
        private void BestScores_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not implemented");
        }
        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
