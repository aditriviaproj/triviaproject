﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;

namespace Client
{

    public partial class MainWindow : Window
    {
        Communicator Com;
        public MainWindow()
        {
        InitializeComponent();
            try
            {
                Com = new Communicator();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error Connecting to server");
                this.Close();
            }
            
        }
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(username.Text) && !string.IsNullOrWhiteSpace(password.Text)) // check user 
            {
                byte[] buff = Serializer.SerializerLogin(username.Text, password.Text);
                byte[] resb = Com.SendMsg(buff);
                byte res = Deserializer.DeserializeLogin(resb);
                    
                if(res != Convert.ToByte('2'))
                {
                    LoggedUser wnd = new LoggedUser(Com);
               
                    this.Hide();
                     wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show("failed logging in");
                }
             
               

            }
            else
            {
                MessageBox.Show("enter username and password");
            }
        }
         private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            SignUp wnd = new SignUp(Com);
            this.Hide();
            wnd.ShowDialog(); ;
        }
        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You need to log in!");
        }
        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You need to log in!");
        }
        private void MyStatus_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You need to log in!");
        }
        private void BestScores_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You need to log in!");
        }
        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
        
    }
}
