﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{

    public partial class JoinRoom : Window
    {
        public List<RoomData> _rooms;
        public Communicator _com { get; set; }

        public JoinRoom(Communicator _com)
        {
            this._com = _com;
            InitializeComponent();
            byte[] resb = _com.SendMsg(Serializer.SerializerGetRooms());
             getRoomRes res = Deserializer.DeserializeGetRooms(resb); 
       
            showALLrooms(res.Rooms);
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            new LoggedUser(_com).ShowDialog();
        }
        private void showALLrooms(List<RoomData> rooms)
        {
            _rooms = rooms;
            for (int i = 0; i < rooms.Count; i++)
            {
                Button btn = new Button();
                btn.Name = rooms[i].name;
                btn.Tag = rooms[i].id;
                btn.Content = rooms[i].name;
                btn.Click += RoomClick;
                RoomStackPanel.Children.Add(btn);
            }
        }

        private void RoomClick(object sender, RoutedEventArgs e)
        {

            int id = Convert.ToInt32((sender as Button).Tag.ToString());
            byte[] resb = _com.SendMsg(Serializer.SerializerJoinRoom(id));
            byte res = Deserializer.DeserializeJoin(resb);
            if (res != Convert.ToByte('2'))
            {
                InRoom wnd = new InRoom(_com, id);
                this.Close();
                wnd.ShowDialog();
            }
            else
            {
                MessageBox.Show(" Room is full :( ");
            }
        }
    }
}
