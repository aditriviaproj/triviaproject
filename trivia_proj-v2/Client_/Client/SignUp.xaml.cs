﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        public Communicator _com { get; set; }
        public SignUp(Communicator com)
        {
            this._com = com;
            InitializeComponent();
        }
        private void SignUp_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(username.Text) && !string.IsNullOrWhiteSpace(password.Text) && !string.IsNullOrWhiteSpace(email.Text))
            {
                byte[] buff = Serializer.SerializerSignUp(username.Text, password.Text, email.Text);
                byte[] resb = _com.SendMsg(buff);
                byte res = Deserializer.DeserializeSignUp(resb);

                if (res != Convert.ToByte('2'))
                {
                    LoggedUser wnd = new LoggedUser(_com);
                    // Window1 wnd = new Window1(username.Text);
                    this.Hide();
                    wnd.ShowDialog();
                }
                else
                {
                    MessageBox.Show("failed to sign up :( ");
                }
            }
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        { 
            this.Close(); 
            new MainWindow().ShowDialog();
        }
        
    }
}
