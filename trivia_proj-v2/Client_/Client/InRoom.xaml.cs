﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for InRoom.xaml
    /// </summary>
    public partial class InRoom : Window
    {
        public int _id { get; set; }
        public Communicator _com { get; set; }
        public InRoom(Communicator _com, int id)
        {
            getplayersINRoom gpir = new getplayersINRoom();
            gpir.ID = (uint)id;
            this._id = id;
            this._com = _com;
            InitializeComponent();
            byte[] buffer = _com.SendMsg(Serializer.SerializerGetPlayersInRoom(gpir));
            GetplayersInRoomsReq players = Deserializer.DeserializeGetPlayersInRooms(buffer);
            showAllPlayers(players);
        
            }
        private void showAllPlayers(GetplayersInRoomsReq Players)
        {
            TextBlock[] tb = new TextBlock[Players.players.Count+1];

            for (int i = 0; i < Players.players.Count; i++)
            {
                tb[i] = new TextBlock();
                tb[i].Text = Players.players[i];

                if (Players.players[i] != Players.admin)
                {
                    tb[i].Foreground = Brushes.White;
                }
                else
                {
                    tb[i].Foreground = Brushes.Aqua;
;
                }
                RoomStackPanel.Children.Add(tb[i]);
            }

        }
    }
}
